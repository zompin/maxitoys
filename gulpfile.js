'use strict'

let gulp 		= require('gulp');
let less 		= require('gulp-less');
let bs 			= require('browser-sync').create();
let sourcemaps 	= require('gulp-sourcemaps');

gulp.task('default', ['bs', 'less'], function() {
	gulp.watch('./*.less', ['less']);
});

gulp.task('bs', function() {
	bs.init({
		notify: false,
		//server: './',
		proxy: 'maxitoys',
		files: ['*/*.php', '*.php', './*.css', './js/*.js']
	});
});

gulp.task('less', function() {
	return gulp.src('./*.less')
		.pipe(sourcemaps.init())
		//.pipe(less({compress:true}))
		.pipe(less())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./'));
});