;(function($) {

	/*Aside*/

	$('.aside__item-header').click(function() {
		$(this).toggleClass('aside__item-header_hidden').next().toggle();
	});

	/*Aside*/

	/*Sorting*/

	$('.sorting__select-current').click(function(e) {
		var current = $(this).next();


		if ($(current).is(':visible')) {
			$(current).hide();
			$(this).removeClass('sorting__select-current_dropped');
		} else {
			$(current).css('display', 'flex');
			$(this).addClass('sorting__select-current_dropped');
		}
	});

	$('.sorting__select-dropdown-item').click(function() {
		var val 	= $(this).attr('data-value') * 1;
		var text 	= $(this).text();
		var select 	= $(this).parent().parent();

		$(select).find('.sorting__value').val(val);
		$(select).find('.sorting__select-current-span').html(text);

		loadProducts();

		if (val == 0) {
			$(select).find('.sorting__select-current').removeClass('sorting__select-current_active');
		} else {
			$(select).find('.sorting__select-current').addClass('sorting__select-current_active');
		}
	});

	$('html').click(function(e) {
		if (!($(e.target).hasClass('sorting__select-current') || $(e.target).hasClass('sorting__select-current-span'))) {
			$('.sorting__select-dropdown').css('display', 'none');
			$('.sorting__select-current').removeClass('sorting__select-current_dropped');
		}
	});

	/*Sorting*/

	/*View*/

	$('.view__button').click(function() {
		var val = $(this).attr('data-value');
		$('.view__button').removeClass('view__button_active');
		$(this).addClass('view__button_active');
		$('.view__mode').val(val);
		$('.product').attr('class', getViewClasses());
	});

	/*View*/

	$('.products').delegate('.product__button_buy', 'click', function() {
		$(this).hide();
		$(this).parent().find('.product__order-hidden').css('display', 'flex');
	});

	loadProducts();
})(jQuery);

function renderTemplate(node) {
	let elem;
	let child;

	if (node instanceof Array) {
		return node.map(function(el) {
			return renderTemplate(el);
		});
	} else if (node instanceof Object) {
		elem = createElement(node);

		if (node.content) child = renderTemplate(node.content);

		if (child instanceof Array) {
			child.forEach(function(el) {
				elem.appendChild(el);
			});
		} else if (child) {
			elem.appendChild(child);
		}

		return elem;
	} else {
		return document.createTextNode(node);
	}
}

function createElement(node) {
	let elem = document.createElement(node.type);
	let prop;

	if (node.classes) elem.className = node.classes;

	if (node.events) {
		if (node.events instanceof Array) {
			node.events.forEach(function(event) {
				elem.addEventListener(event.type, event.callback);
			});
		} else {
			elem.addEventListener(node.events.type, node.events.callback);
		}
	}

	for (prop in node.attr) {
		if (node.attr[prop]) elem.setAttribute(prop, node.attr[prop]);
	}

	return elem;
}

function loadProducts() {
	$('.products').addClass('products_loading').empty();
	var price 	= $('.sorting__value_price').val() * 1;
	var size 	= $('.sorting__value_size').val() * 1;
	var brand 	= $('.sorting__value_brand').val() * 1;
	var url 	= '/data.json' + '?price=' + price + '&size=' + size + '&brand=' + brand;

	$.ajax({
		type: 'GET',
		url: url,
		dataType: 'json',
		success: function(data) {
			setTimeout(function() {
				renderProducts(data);
				$('.products').removeClass('products_loading');
			}, 300);
		},
		error: function() {
			alert("Не удалось загрузить данные с сервера");
			$('.products').removeClass('products_loading');
		}
	});
}

function getViewClasses() {
	var classes = '';
	var view = $('.view__mode').val() * 1;

	switch (view) {
		case 0:
			classes = 'product product_medium';
		break;
		case 1:
			classes = 'product product_list';
		break;
		case 2:
			classes = 'product product_large';
		break;
		case 3:
			classes = 'product product_small';
		break;
	}

	return classes;
}

function renderProducts(data) {
	var productsE = $('.products');
	productsE.empty();

	data.forEach(function(product) {
		var template = {
				type: 'div',
				classes: getViewClasses(),
				content: [
					{
						type: 'div',
						classes: 'product__thumb',
						content: {
							type: 'img',
							classes: 'product__thumb-img',
							attr: {src: product.thumb}
						}
					}, {
						type: 'div',
						classes: 'product__desc',
						content: [
							{
								type: 'a',
								classes: 'product__title',
								content: product.title,
								attr: {href: '#'}
							}, {
								type: 'div',
								classes: 'product__vcode',
								content: product.vcode
							}
						]
					}, {
						type: 'div',
						classes: 'product__price',
						content: [
							{
								type: 'span',
								classes: 'product__price-value',
								content: product.price
							}, {
								tupe: 'span',
								classes: 'product__price-currency',
								content: 'ю'
							}
						]
					}, {
						type: 'div',
						classes: 'product__size',
						content: [
							{
								type: 'span',
								classes: 'product__size-size',
								content: 'Размер:'
							}, {
								type: 'span',
								classes: 'product__size-value',
								content: product.size
							}, {
								type: 'span',
								classes: 'product__size-unit',
								content: 'см'
							}
						]
					}, {
						type: 'div',
						classes: 'product__order',
						content: [
							{
								type: 'button',
								classes: 'product__button product__button_buy',
								content: 'Купить'
							}, {
								type: 'div',
								classes: 'product__order-hidden',
								content: [
									{
										type: 'span',
										classes: 'product__x',
										content: '×'
									},{
										type: 'input',
										classes: 'product__amount'
									}, {
										type: 'div',
										classes: 'product__price product__price_hidden',
										content: [
											{
												type: 'span',
												classes: 'product__price-value',
												content: product.price
											}, {
												tupe: 'span',
												classes: 'product__price-currency',
												content: 'ю'
											}
										]
									}, {
										type: 'button',
										classes: 'product__button product__button_add',
										content: [
											{
												type: 'span',
												classes: 'product__button-span',
												content: 'Купить '
											}, {
												type: 'span',
												classes: 'product__button-span product__button-amount',
											}
										]
									}
								]
							}
						]
					}
				]
		};
		productsE.append(renderTemplate(template));
	});
}